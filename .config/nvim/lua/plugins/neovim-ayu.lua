return {
    'Shatur/neovim-ayu',
    as = "ayu",
    config = function()
        vim.cmd('colorscheme ayu-dark')
    end
}
