return {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.5',
    dependencies = { 'nvim-lua/plenary.nvim' },
    config = function()
        require("telescope").setup({

        })
    end,
    keys = {
        { "<leader>ff", function() require('telescope.builtin').find_files() end, { noremap = true, desc = "Find files" }, },
        { "<leader>fg", function() require('telescope.builtin').live_grep() end,  { noremap = true, desc = "Grep files" } },
        { "<leader>fh", function() require('telescope.builtin').help_tags() end,  { noremap = true, desc = "Find help" } },
        { "<leader>fB", function() require('telescope.builtin').buffers() end,    { noremap = true, desc = "Find buffers" } },
        { '<leader>ps', function()
            require('telescope.builtin').grep_string({ search = vim.fn.input("Grep > ") })
        end,
        }
    }
}
