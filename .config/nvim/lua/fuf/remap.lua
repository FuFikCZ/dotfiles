vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)


function render_jsonnet(file)
    vim.cmd("w")
    vim.cmd("!jsonnet -J vendor -J lib " .. file .. " | yq -P")
    vim.cmd("vsplit")
end

vim.api.nvim_set_keymap("n", "<Leader>fj", "<cmd>lua render_jsonnet('" .. vim.api.nvim_buf_get_name(0) .. "')<CR>",
    { noremap = true })
